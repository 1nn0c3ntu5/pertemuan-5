// Soal No 1
function cetakFunction(){
    return "hai namaku innocentus ananda"
}
console.log(cetakFunction())

console.log("\n")

// Soal No 2
function myFunction(angka1, angka2){
    return angka1+angka2
}
let angka1 = 30
let angka2 = 5
let output = myFunction(angka1, angka2)
console.log(output)

console.log("\n")

// Soal No 3
const Hello = () => {
    return "Hello"
}
console.log(Hello())

console.log("\n")

// Soal No 4
let obj = {
    nama : "bahrun",
    umur : 35,
    bahasa : "indonesia"
}
console.log(obj.bahasa)

console.log("\n")

// Soal No 5
let arrayDaftarPeserta = ["bahrun", "laki-laki", "gitaris" , 1998]
let objDaftarPeserta = {
    nama : arrayDaftarPeserta[0],
    jeniskelamin : arrayDaftarPeserta[1],
    hobi : arrayDaftarPeserta[2],
    tahunlahir : arrayDaftarPeserta[3]
}
console.log(objDaftarPeserta)

console.log("\n")

// Soal No 6
var buah = [
    {nama: "Nanas", warna: "Kuning", adaBijinya: "false", harga: 9000},
    {nama: "Jeruk", warna: "Oranye", adaBijinya: "true", harga: 8000},
    {nama: "Semangka", warna: "Hijau & Merah", adaBijinya: "false", harga: 10000},
    {nama: "Pisang", warna: "Kuning", adaBijinya: "false", harga: 5000}
]
var result = buah.filter(function(buahh){
    return buahh.adaBijinya == "false"
})

console.log(result)

console.log("\n")

// Soal No 7
let phone = {
    name: "xiaomi pro 10",
    brand: "xiaomi",
    year: 2021
 }
 const {name, brand, year} = phone
 
 console.log(name, brand, year) 

 console.log("\n")

 // Soal No 8
 let dataBukuTambahan= {
    penulis: "bahrun",
    tahunTerbit: 2020 
  }
  
  let buku = {
    nama: "tecno",
    jumlahHalaman: 172
  }
  let objOutput = {...dataBukuTambahan, ...buku}
  console.log(objOutput) 

  console.log("\n")

// Soal No 9
let mobil = {
    merk : "pajero",
    color: "white",
    year : 2020
}
    
const functionObject = (param) => {    
    return param
}
console.log(functionObject(mobil))